package com.lorenz_niels.guestbook.components;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class UserAuthProvider {

    private final UserService service;

    public UserAuthProvider(final UserService service) {
        this.service = service;
    }

    private String getCurrentUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication==null?null:authentication.getName();
    }

    public User getCurrentUserObjectByName(String username) {
        return service.findByName(username);
    }

    public User getCurrentUserObject() {
        String username = this.getCurrentUserName();
        return username==null?null:this.getCurrentUserObjectByName(username);
    }
}
