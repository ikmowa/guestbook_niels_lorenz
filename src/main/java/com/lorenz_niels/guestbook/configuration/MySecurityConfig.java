package com.lorenz_niels.guestbook.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.endpoint.NimbusAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import javax.sql.DataSource;


@Configuration
@EnableWebSecurity
@ComponentScan
@Order(1000)
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthorizationRequestRepository<OAuth2AuthorizationRequest> authorizationRequestRepository() {
        return new HttpSessionOAuth2AuthorizationRequestRepository();
    }

    @Bean
    public OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient() {
        return new NimbusAuthorizationCodeTokenResponseClient();
    }

    @Bean
    public WebSecurityConfigurer<WebSecurity> securityConfigurer(DataSource ds) {
        return new WebSecurityConfigurerAdapter() {
            @Override
            protected void configure(AuthenticationManagerBuilder auth) throws Exception {
                auth.jdbcAuthentication()
                        .passwordEncoder(new BCryptPasswordEncoder())
                        .dataSource(ds)
                        .usersByUsernameQuery("select username, password, enabled from user where username = ?")
                        .authoritiesByUsernameQuery("select username, role from user where username = ?");
            }

            @Override
            protected void configure(HttpSecurity http) throws Exception {

                http.csrf().disable().authorizeRequests().antMatchers("/**")
                        .authenticated()
                        .and()
                        .formLogin()
                        .loginPage("/login")
                        .loginProcessingUrl("/authenticateUser")
                        .permitAll()
                        .and()
                        .logout()
                        .permitAll()
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                        .and()
                        .exceptionHandling()
                        .accessDeniedPage("/error")
                        .and()
                        .oauth2Login()
                        .loginPage("/login/oauth2")
//                        .defaultSuccessUrl("/")
                        .failureUrl("/loginFailure")
                        .tokenEndpoint()
                        .accessTokenResponseClient(accessTokenResponseClient())
                        .and()
                        .authorizationEndpoint()
                        .baseUri("/oauth2/authorize")
                        .authorizationRequestRepository(authorizationRequestRepository())
                ;
            }

            @Override
            public void configure(WebSecurity webSecurity) throws Exception {
                webSecurity
                        .ignoring()
                        .antMatchers("/resources/**", "/register/", "/", "/login/**", "/auth/**", "/oauth_login/**", "/oath2/**");
            }
        };


    }
}


