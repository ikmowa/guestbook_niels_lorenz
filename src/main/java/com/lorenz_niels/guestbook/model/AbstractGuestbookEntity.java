package com.lorenz_niels.guestbook.model;

import javax.persistence.*;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractGuestbookEntity {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
