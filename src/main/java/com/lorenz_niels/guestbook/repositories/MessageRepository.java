package com.lorenz_niels.guestbook.repositories;

import com.lorenz_niels.guestbook.model.Messages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MessageRepository extends JpaRepository<Messages, UUID> {
}
