package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.GuestBook;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GuestBookService {

    Optional<GuestBook> getGuestBookByUUID(final UUID uuid);

    List<GuestBook> getAllGuestBooks();

    GuestBook createNewGuestBook(GuestBook guestBook);

    GuestBook updateGuestBook(GuestBook guestBook);

    void removeGuestBook(UUID id);
}
