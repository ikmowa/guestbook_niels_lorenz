package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.Messages;

import java.util.UUID;

public interface MessageService {
    Messages createNewMessage(Messages mapMessagesDTOtoMessagesEntity, UUID uuid);

    void deleteMessage(UUID uuid);
}
