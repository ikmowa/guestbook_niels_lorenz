package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.repositories.GuestBookRepository;
import com.lorenz_niels.guestbook.services.exceptions.GuestBookServiceException;
import org.springframework.stereotype.Service;
import com.lorenz_niels.guestbook.model.GuestBook;

import java.util.*;

@Service
public class RepositoryBasedGuestBookService implements GuestBookService {

    private final GuestBookRepository repository;

    public RepositoryBasedGuestBookService(final GuestBookRepository repository) {
        this.repository = repository;
    }

    public Optional<GuestBook> getGuestBookByUUID(final UUID uuid) {
        return repository.findById(uuid);
    }

    public List<GuestBook> getAllGuestBooks() {
        return repository.findAll();
    }

    public GuestBook createNewGuestBook(final GuestBook guestBook) {
        return repository.save(guestBook);
    }

    @Override
    public GuestBook updateGuestBook(final GuestBook guestBook) {
        Optional<GuestBook> maybeBook = repository.findById(guestBook.getId());
        if (maybeBook.isPresent()) {
            return repository.save(maybeBook.get());
        } else {
            throw new GuestBookServiceException("The guestbook " + guestBook + " you tried to update does not exist.");
        }
    }

    @Override
    public void removeGuestBook(final UUID id) {
        Optional<GuestBook> maybeBook = repository.findById(id);
        if (maybeBook.isPresent()) {
            repository.delete(maybeBook.get());
        } else {
            throw new GuestBookServiceException("The guestbook with id " + id + " you tried to delete does not exist.");
        }
    }
}
