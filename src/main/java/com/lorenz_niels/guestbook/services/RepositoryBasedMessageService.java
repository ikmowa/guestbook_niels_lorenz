package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.components.UserAuthProvider;
import com.lorenz_niels.guestbook.model.Messages;
import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.MessageRepository;
import com.lorenz_niels.guestbook.services.exceptions.MessageServiceException;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
public class RepositoryBasedMessageService implements MessageService{
    private final MessageRepository repository;
    private final GuestBookService guestBookService;
    private final UserAuthProvider authProvider;

    public RepositoryBasedMessageService(final MessageRepository repository, final GuestBookService service,
                                         final UserAuthProvider authProvider) {
        this.repository = repository;
        this.guestBookService = service;
        this.authProvider = authProvider;
    }


    public Messages createNewMessage(final Messages messages, final UUID uuid) {
        User user = authProvider.getCurrentUserObject();
        if (user==null) {
            throw new NoSuchElementException("Auth user is not found.");
        }
        messages.setUser(user);
        messages.setGuestbook(guestBookService.getGuestBookByUUID(uuid).orElseThrow());
        return repository.save(messages);
    }

    public void deleteMessage(final UUID uuid) {
        Optional<Messages> maybeMessage = repository.findById(uuid);
        if (maybeMessage.isPresent()) {
            repository.delete(maybeMessage.get());
        } else {
            throw new MessageServiceException("The message with id " + uuid + " you tried to delete does not exist.");
        }
    }
}
