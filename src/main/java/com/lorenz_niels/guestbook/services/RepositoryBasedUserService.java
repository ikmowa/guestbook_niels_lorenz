package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.components.UserAuthProvider;
import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.UserRepository;
import com.lorenz_niels.guestbook.services.exceptions.UserServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RepositoryBasedUserService implements UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;


    public RepositoryBasedUserService(final UserRepository repository, final PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<User> findUserById(final UUID id) {
        return repository.findById(id);
    }

    @Override
    public List<User> findAllUsers() {
        return repository.findAll();
    }

    @Override
    public User save(final User user) {
        return repository.save(user);
    }

    @Override
    public User update(final User user) {
        Optional<User> maybeUser = repository.findById(user.getId());
        if (maybeUser.isPresent()) {
            return repository.save(maybeUser.get());
        } else {
            throw new UserServiceException("The user " + user + " you tried to update does not exist.");
        }
    }

    @Override
    public void delete(final UUID id) {
        Optional<User> maybeUser = repository.findById(id);
        if (maybeUser.isPresent()) {
            repository.delete(maybeUser.get());
        } else {
            throw new UserServiceException("The user with id " + id + " you tried to delete does not exist.");
        }
    }

    @Override
    public User findByName(final String name) {
        Optional<User> maybeUser = repository.findByUsername(name);
        if (maybeUser.isPresent()) {
            return maybeUser.get();
        } else {
            throw new UserServiceException("User with username " + name + " does not exist.");
        }
    }

    private String[] strings = {"login", "logout", "index", "register",
            "detail", "list", "user", "guestbook", "403", "404"};
    private List<String> reservedWords = new ArrayList<>();

    @Override
    public Boolean makeNewUser(final User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        reservedWords.addAll(Arrays.asList(strings));
        boolean userCreated = false;

        try {
            Optional<User> usernameFreeTest = repository.findByUsername(user.getUsername());
            if (usernameFreeTest.isPresent() || reservedWords.stream().anyMatch(n -> n.equals(user.getUsername()))) {
                throw new ReservedWordUsernameException("Username can not be a reserved word.");
            } else {
                repository.save(user);
                userCreated = true;
            }
        } catch (ReservedWordUsernameException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userCreated;
    }
}
