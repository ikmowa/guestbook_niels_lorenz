package com.lorenz_niels.guestbook.services;

public class ReservedWordUsernameException extends RuntimeException {
    public ReservedWordUsernameException() {
        super();
    }

    public ReservedWordUsernameException(String message) {
        super(message);
    }

    public ReservedWordUsernameException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReservedWordUsernameException(Throwable cause) {
        super(cause);
    }

    protected ReservedWordUsernameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
