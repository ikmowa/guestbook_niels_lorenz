package com.lorenz_niels.guestbook.services.exceptions;

public class GuestBookServiceException extends RuntimeException {
    public GuestBookServiceException(String s) {
        super(s);
    }

    public GuestBookServiceException() {
        super();
    }

    public GuestBookServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GuestBookServiceException(final Throwable cause) {
        super(cause);
    }

    protected GuestBookServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
