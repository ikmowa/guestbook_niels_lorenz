package com.lorenz_niels.guestbook.services.exceptions;

public class MessageServiceException extends RuntimeException {
    public MessageServiceException(final String s) {
        super(s);
    }

    public MessageServiceException() {
        super();
    }

    public MessageServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public MessageServiceException(final Throwable cause) {
        super(cause);
    }

    protected MessageServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
