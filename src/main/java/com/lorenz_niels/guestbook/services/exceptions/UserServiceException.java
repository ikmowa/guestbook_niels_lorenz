package com.lorenz_niels.guestbook.services.exceptions;

public class UserServiceException extends RuntimeException {
    public UserServiceException(final String s) {
        super(s);
    }

    public UserServiceException() {
        super();
    }

    public UserServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserServiceException(final Throwable cause) {
        super(cause);
    }

    protected UserServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
