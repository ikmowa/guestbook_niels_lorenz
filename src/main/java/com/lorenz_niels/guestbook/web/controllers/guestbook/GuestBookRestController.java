package com.lorenz_niels.guestbook.web.controllers.guestbook;

import com.lorenz_niels.guestbook.DatabaseGeneratorComponent;
import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import com.lorenz_niels.guestbook.web.model.validation.PostValidation;
import com.lorenz_niels.guestbook.web.model.validation.PutValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/guestbook")
public class GuestBookRestController {

    @Autowired
    DatabaseGeneratorComponent dbscript;

    private final GuestBookDelegateController delegate;

    public GuestBookRestController(final GuestBookDelegateController delegate) {
        this.delegate = delegate;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuestBookDTO> getGuestBook(@PathVariable("id") final UUID uuid) {
        return new ResponseEntity<>(delegate.getGuestBook(uuid), HttpStatus.OK);
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GuestBookDTO> getAllBooks() {
        dbscript.run();
        return delegate.getAllGuestBooks();
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuestBookDTO> postGuestBook(@RequestBody @Validated(PostValidation.class) final GuestBookDTO guestBookDTO) {
        return new ResponseEntity<>(delegate.postGuestBook(guestBookDTO), HttpStatus.CREATED);
    }

    @PutMapping(value = "/")
    public ResponseEntity<GuestBookDTO> putGuestBook(@RequestBody @Validated(PutValidation.class) final GuestBookDTO guestBookDTO) {
        return new ResponseEntity<>(delegate.putGuestBook(guestBookDTO), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<GuestBookDTO> deleteGuestBook(@PathVariable("id") final UUID id) {
        delegate.deleteGuestBook(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
