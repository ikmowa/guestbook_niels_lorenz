package com.lorenz_niels.guestbook.web.controllers.mappers;

import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import org.mapstruct.Mapper;
import com.lorenz_niels.guestbook.model.GuestBook;

import java.util.List;

@Mapper(uses = {UUIDMapper.class, UserNameMapper.class})
public interface GuestBookMapper {

    GuestBookDTO mapGuestBookEntityToGuestBookDTO(GuestBook guestBook);

    GuestBook mapGuestBookDTOtoGuestBookEntity(GuestBookDTO guestBookDTO);

    List<GuestBookDTO> mapGuestBookEntityToGuestBookDTO(List<GuestBook> guestBookDTO);
}
