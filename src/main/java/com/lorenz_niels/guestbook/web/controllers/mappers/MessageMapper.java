package com.lorenz_niels.guestbook.web.controllers.mappers;

import com.lorenz_niels.guestbook.model.GuestBook;
import com.lorenz_niels.guestbook.model.Messages;
import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import com.lorenz_niels.guestbook.web.model.MessagesDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = {UUIDMapper.class,UserNameMapper.class})
public interface MessageMapper {

    MessagesDTO mapMessagesEntityToMessagesDTO(Messages messages);

    Messages mapMessagesDTOtoMessagesEntity(MessagesDTO messagesDTO);

    List<MessagesDTO> mapMessagesEntityToMessagesDTO(List<Messages> messagesDTO);
}
