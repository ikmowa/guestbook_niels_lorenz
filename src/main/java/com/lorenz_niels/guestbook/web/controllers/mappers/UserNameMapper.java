package com.lorenz_niels.guestbook.web.controllers.mappers;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserNameMapper {
    @Autowired
    UserService service;

    public User stringToUser(String string) {
        return string == null ? null : service.findByName(string);
    }

    public String userToString(User user) {
        return user == null ? null : user.getUsername();
    }
}