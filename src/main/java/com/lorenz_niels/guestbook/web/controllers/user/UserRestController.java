package com.lorenz_niels.guestbook.web.controllers.user;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.services.UserService;
import com.lorenz_niels.guestbook.web.controllers.mappers.UserMapper;
import com.lorenz_niels.guestbook.web.model.UserDTO;
import com.lorenz_niels.guestbook.web.model.validation.PostValidation;
import com.lorenz_niels.guestbook.web.model.validation.PutValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserRestController {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserRestController(final UserService userService, final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") final UUID id) {
        return userService.findUserById(id)
                .map(userMapper::mapUserEntityToUserDTO)
                .map(userDTO -> new ResponseEntity<>(userDTO, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDTO> getAllUsers() {
        return userMapper.mapUsersToDTOs(userService.findAllUsers());
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> postUser(@RequestBody @Validated(PostValidation.class) final UserDTO userDTO) {
        User savedUser = userService.save(userMapper.mapUserDTOToUserEntity(userDTO));
        return new ResponseEntity<>(userMapper.mapUserEntityToUserDTO(savedUser), HttpStatus.CREATED);
    }

    @PutMapping(value = "/")
    public ResponseEntity<UserDTO> putUser(@RequestBody @Validated(PutValidation.class) final UserDTO userDTO) {
        User userUpdated = userService.update(userMapper.mapUserDTOToUserEntity(userDTO));
        return new ResponseEntity<>(userMapper.mapUserEntityToUserDTO(userUpdated), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UserDTO> deleteUser(@PathVariable("id") final UUID id) {
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}