package com.lorenz_niels.guestbook.web.model;

import com.lorenz_niels.guestbook.model.Messages;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalTime;

public class MessagesDTO extends AbstractGuestbookDTO implements Comparable<MessagesDTO> {

    @NotBlank
    private String title;

    private String comment;

    @NotBlank
    private String user;

    @PastOrPresent
    private LocalTime date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalTime getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDate(LocalTime date) {
        this.date = date;
    }

    @Override
    public int compareTo(MessagesDTO o) {
        return getDate().compareTo(o.getDate());
    }
}
