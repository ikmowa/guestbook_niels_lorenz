package com.lorenz_niels.guestbook.components;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.UserRepository;
import com.lorenz_niels.guestbook.services.RepositoryBasedUserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserAuthProviderTest {

        @Mock
        RepositoryBasedUserService service;

        @InjectMocks
        UserAuthProvider authProvider;

    @Test
    void getCurrentUserObjectByNameSuccesFull() {
        User user = new User();
        user.setUsername("Nielsstrychi");
        when(service.findByName("Nielsstrychi")).thenReturn(user);
        assertEquals(user,authProvider.getCurrentUserObjectByName(user.getUsername()));
    }
}