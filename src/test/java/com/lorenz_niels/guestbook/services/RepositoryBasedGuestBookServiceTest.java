package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.GuestBook;
import com.lorenz_niels.guestbook.repositories.GuestBookRepository;
import com.lorenz_niels.guestbook.services.exceptions.GuestBookServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RepositoryBasedGuestBookServiceTest {

    @Mock
    GuestBookRepository repo;

    @InjectMocks
    RepositoryBasedGuestBookService service;

    @Test
    void getGuestBookByUUIDShouldReturnGuestBook() {
        GuestBook guestBook = new GuestBook();
        UUID guestBookId = UUID.randomUUID();
        guestBook.setId(guestBookId);
        when(repo.findById(guestBookId)).thenReturn(Optional.of(guestBook));
        assertThat(Optional.of(guestBook).get()).isSameAs(service.getGuestBookByUUID(guestBookId).get());
    }

    @Test
    void ifGetGuestBookByUUIDIsNotFoundShouldReturnOptionalOfEmpty() {
        GuestBook guestBook = new GuestBook();
        UUID guestBoookId = UUID.randomUUID();
        guestBook.setId(guestBoookId);
        when(repo.findById(guestBoookId)).thenReturn(Optional.empty());
        assertThat(Optional.empty()).isSameAs(service.getGuestBookByUUID(guestBoookId));
    }

    @Test
    void getAllGuestBooksShouldReturnListOfGuestBooks() {
        List<GuestBook> guestBookList = new ArrayList<>();
        GuestBook guestBook = new GuestBook();
        guestBookList.add(guestBook);
        when(repo.findAll()).thenReturn(guestBookList);
        assertThat(guestBookList).isEqualTo(service.getAllGuestBooks());
    }

    @Test
    void createNewGuestBookShouldAddGuestBookByRepository() {
        GuestBook guestBook = new GuestBook();
        UUID guestBookId = UUID.randomUUID();
        guestBook.setId(guestBookId);
        GuestBook expected = service.createNewGuestBook(guestBook);
        lenient().when(repo.findById(guestBookId)).thenReturn(Optional.ofNullable(expected));
        verify(repo).save(guestBook);
    }

    @Test
    void updateGuestBookShouldFindAndUpdateGuestBookByRepository() {
        GuestBook guestBookToUpdate = new GuestBook();
        UUID guestBookId = UUID.randomUUID();
        guestBookToUpdate.setId(guestBookId);
        GuestBook expected = new GuestBook();
        when(repo.findById(guestBookId)).thenReturn(Optional.of(expected));
        assertThat(service.getGuestBookByUUID(guestBookId).get()).isSameAs(Optional.of(expected).get());
        service.updateGuestBook(guestBookToUpdate);
        verify(repo).save(expected);
    }

    @Test
    void updateGuestBookShouldThrowExceptionIfGuestBookIsNotPresent() {
        GuestBook guestBook = new GuestBook();
        when(repo.findById(guestBook.getId())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.updateGuestBook(guestBook))
                .isInstanceOf(GuestBookServiceException.class)
                .hasMessage("The guestbook " + guestBook + " you tried to update does not exist.");
    }

    @Test
    void removeGuestBookShouldDeleteGuestBookByRepository() {
        GuestBook guestBookToDelete = new GuestBook();
        UUID guestBookId = UUID.randomUUID();
        guestBookToDelete.setId(guestBookId);
        try {
            service.removeGuestBook(guestBookId);
            verify(repo).delete(guestBookToDelete);
        } catch (GuestBookServiceException e) {
        }
    }

    @Test
    void removeGuestBookShouldThrowExceptionIfGuestBookIdIsNotFound() {
        UUID guestBookId = UUID.randomUUID();
        when(repo.findById(guestBookId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.removeGuestBook(guestBookId))
                .isInstanceOf(GuestBookServiceException.class)
                .hasMessage("The guestbook with id " + guestBookId + " you tried to delete does not exist.");
    }
}