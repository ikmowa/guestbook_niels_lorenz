package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.components.UserAuthProvider;
import com.lorenz_niels.guestbook.model.GuestBook;
import com.lorenz_niels.guestbook.model.Messages;
import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.MessageRepository;
import com.lorenz_niels.guestbook.services.exceptions.MessageServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RepositoryBasedMessageServiceTest {

    @Mock
    MessageRepository repo;

    @Mock
    UserAuthProvider authProvider;

    @InjectMocks
    RepositoryBasedMessageService messageService;

    @Test
    void createNewMessageShouldAddMessageOnUserAndGuestBookByRepository() {
        User user = new User();
        GuestBook guestBook = new GuestBook();
        UUID guestBookId = UUID.randomUUID();
        guestBook.setId(guestBookId);
        Messages message = new Messages();
        message.setUser(user);
        message.setGuestbook(guestBook);
        try {
            messageService.createNewMessage(message, guestBookId );
            when(repo.findById(guestBookId)).thenReturn(Optional.empty());
            verify(repo).save(message);
        } catch (NoSuchElementException e) {
        }
    }

    @Test
    void deleteMessageShouldDeleteMessageByRepository() {
        Messages messagesToDelete = new Messages();
        UUID messagesId = UUID.randomUUID();
        messagesToDelete.setId(messagesId);
        try {
            messageService.deleteMessage(messagesId);
            verify(repo).delete(messagesToDelete);
        } catch (MessageServiceException e) {
        }
    }

    @Test
    void deleteMessageShouldThrowExceptionIfNotFound() {
        UUID messageId = UUID.randomUUID();
        when(repo.findById(messageId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> messageService.deleteMessage(messageId))
                .isInstanceOf(MessageServiceException.class)
                .hasMessage("The message with id " + messageId + " you tried to delete does not exist.");
    }
}