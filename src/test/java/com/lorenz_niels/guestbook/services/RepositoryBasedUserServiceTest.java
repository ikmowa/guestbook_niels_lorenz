package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.UserRepository;
import com.lorenz_niels.guestbook.services.exceptions.UserServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RepositoryBasedUserServiceTest {

    @Mock
    UserRepository repo;

    @Mock
    PasswordEncoder passwordEncoder;

    @InjectMocks
    RepositoryBasedUserService service;

    @Test
    void findUserByIdShouldReturnUser() {
        User user = new User();
        UUID userId = UUID.randomUUID();
        user.setId(userId);
        when(repo.findById(userId)).thenReturn(Optional.of(user));
        assertThat(Optional.of(user).get()).isSameAs(service.findUserById(userId).get());
    }

    @Test
    void ifFindUserByIdIsNotFoundShouldReturnOptionalOfEmpty() {
        User user = new User();
        UUID userId = UUID.randomUUID();
        user.setId(userId);
        when(repo.findById(userId)).thenReturn(Optional.empty());
        assertThat(Optional.empty()).isSameAs(service.findUserById(userId));
    }

    @Test
    void findByNameShouldReturnUser() {
        User user = new User();
        user.setUsername("Lorenz");
        when(repo.findByUsername("Lorenz")).thenReturn(Optional.of(user));
        assertThat(user).isEqualTo(service.findByName("Lorenz"));
    }

    @Test
    void ifFindByNameIsNotFoundShouldThrowException() {
        when(repo.findByUsername("Lorenz")).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.findByName("Lorenz"))
                .isInstanceOf(UserServiceException.class)
                .hasMessage("User with username Lorenz does not exist.");

    }

    @Test
    void findAllUsersShouldReturnListOfUsers() {
        List<User> userList = new ArrayList<>();
        User user = new User();
        userList.add(user);
        when(repo.findAll()).thenReturn(userList);
        assertThat(userList).isEqualTo(service.findAllUsers());
    }

    @Test
    void saveShouldAddUserByRepository() {
        User user = new User();
        user.setUsername("Lorenz");
        service.save(user);
        verify(repo).save(user);
    }

    @Test
    void updateShouldFindAndUpdateUserByRepository() {
        User userToUpdate = new User();
        UUID userId = UUID.randomUUID();
        userToUpdate.setId(userId);
        User expected = new User();
        when(repo.findById(userId)).thenReturn(Optional.of(expected));
        assertThat(service.findUserById(userId).get()).isSameAs(Optional.of(expected).get());
        service.update(userToUpdate);
        verify(repo).save(expected);
    }

    @Test
    void updateShouldThrowExceptionIfUserIsNotPresent() {
        User user = new User();
        when(repo.findById(user.getId())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.update(user))
                .isInstanceOf(UserServiceException.class)
                .hasMessage("The user " + user + " you tried to update does not exist.");
    }


    @Test
    void deleteShouldRemoveUserByRepository() {
        User userToDelete = new User();
        UUID userId = UUID.randomUUID();
        userToDelete.setId(userId);
        try {
            service.delete(userId);
            verify(repo).delete(userToDelete);
        } catch (UserServiceException e) {
        }
    }

    @Test
    void deleteShouldThrowExceptionIfUserIdIsNotPresent() {
        UUID userId = UUID.randomUUID();
        when(repo.findById(userId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.delete(userId))
                .isInstanceOf(UserServiceException.class)
                .hasMessage("The user with id " + userId + " you tried to delete does not exist.");

    }

    @Test
    void makeNewUserIsReservedWord() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername("login");
        assertThatThrownBy(() -> service.makeNewUser(user))
                .isInstanceOf(ReservedWordUsernameException.class);
    }

    @Test
    void makeNewUserSucces() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername("Niels1");
        when(repo.findByUsername(user.getUsername())).thenReturn(Optional.empty());
        assertTrue(service.makeNewUser(user));
    }
}